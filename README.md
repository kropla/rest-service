REST microservice exmple

###1. Technologies used
```
* Maven 3
* Spring-Boot 
* *spring v4, spring-data-jpa
* h2 embedded database
```

###2. To Run this project locally
```shell
$ git clone https://gitlab.com/kroplewski/rest-service.git
$ mvn clean install
$ mvn spring-boot:run

While application is starting the initial database script is triggered. It creates user accounts with some dummy transaction history
USERS IDs:
1,2,3

```

###3. Available services
```
localhost:8080/rest/history/user/{userId}
localhost:8080/rest/balance/user/{userId}
localhost:8080/rest/balance/increase/user/{userId}
localhost:8080/rest/balance/decrease/user/{userId}
localhost:8080/rest/tokens/user/{userId}

On the boot-secure branch implemented version with secure connection to service base on basic auth
https://localhost:8443/rest/history/user/{userId}
https://localhost:8443/rest/balance/user/{userId}
https://localhost:8443/rest/balance/increase/user/{userId}
https://localhost:8443/rest/balance/decrease/user/{userId}
https://localhost:8443/rest/tokens/user/{userId}
```
user:admin
password: lukrecja