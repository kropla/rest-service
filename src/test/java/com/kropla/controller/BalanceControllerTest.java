package com.kropla.controller;

import com.kropla.domain.entity.Account;
import com.kropla.service.BalanceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by com.kropla.kropla on 31.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest

@ActiveProfiles("scratch")
public class BalanceControllerTest {
    private static final String CURRENT_BALANCE_URL = "/balance/user/";

    @Autowired
    private WebApplicationContext context;

    @InjectMocks
    private BalanceController ballanceController;

    @Mock
    private BalanceService balanceService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void shouldReturnBalance_AmountForUser() throws Exception {
        Account mockAcc = new Account();
        mockAcc.setBalance(100);

        given(balanceService.getUserAccount("1")).willReturn(mockAcc);

        this.mvc.perform(get(CURRENT_BALANCE_URL + "4"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.*", hasSize(1)))
                .andExpect(jsonPath("$.value", is(145)));
    }

    @Test
    public void shouldReturnNoContent_ForNotExistingUser() throws Exception {
        this.mvc.perform(get(CURRENT_BALANCE_URL + "99"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturnNoFound_ForEmptyUserId() throws Exception {
        this.mvc.perform(get(CURRENT_BALANCE_URL))
                .andExpect(status().isNotFound());
    }

}
