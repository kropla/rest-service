package com.kropla.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.GreaterThan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by com.kropla.kropla on 31.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest

@ActiveProfiles("scratch")
public class HistoryControllerTest {
    private static final String HISTORY_URL = "/history/user/";

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }


    @Test
    public void shouldReturnListOfTransactions() throws Exception {

        this.mvc.perform(get(HISTORY_URL + "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.*", hasSize(1)))
                .andExpect(jsonPath("$.transactions", hasSize(new GreaterThan<>(3))))
                .andExpect(jsonPath("$.transactions.[0].type", notNullValue()))
                .andExpect(jsonPath("$.transactions.[0].type", is("increase")))
                .andExpect(jsonPath("$.transactions.[0].value", notNullValue()))
                .andExpect(jsonPath("$.transactions.[0].value", is(100)))
                .andExpect(jsonPath("$.transactions.[3].type", notNullValue()))
                .andExpect(jsonPath("$.transactions.[3].type", is("decrease")))
                .andExpect(jsonPath("$.transactions.[3].value", notNullValue()))
                .andExpect(jsonPath("$.transactions.[3].value", is(103)));
    }

    @Test
    public void shouldReturnNoContent_forNotExistingUser() throws Exception {

        this.mvc.perform(get(HISTORY_URL + "99"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldReturnEmptyList_forUserWithoutTransactions() throws Exception {

        this.mvc.perform(get(HISTORY_URL + "4"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.*", hasSize(1)))
                .andExpect(jsonPath("$.transactions", hasSize(0)));
    }
}
