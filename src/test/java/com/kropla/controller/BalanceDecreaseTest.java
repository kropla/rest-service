package com.kropla.controller;

import com.kropla.service.TokenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by com.kropla.kropla on 31.12.16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest

@ActiveProfiles("scratch")
public class BalanceDecreaseTest {
    private static final String DECREASE_BALANCE_URL = "/balance/decrease/user/";

    @Autowired
    private TokenService tokenService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }


    @Test
    public void shouldReturn422_WhenDecreasingBalanceWithoutToken() throws Exception {
        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":102}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturn400_WhenDecreasingBalanceWithEmptyBody() throws Exception {
        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturn422_WhenDecreasingBalanceWithWrongAmountFormat() throws Exception {
        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":wer}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturn422_WhenDecreasingBalanceWithWrongContentAttrib() throws Exception {
        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"values\":100}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string(""));
    }


    @Test
    public void shouldReturn401_WhenDecreasingBalanceWithInvalidToken() throws Exception {
        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":100,\"token\" : \"JT8HQ8\"}"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturn400_WhenDecreasingBalanceWithNotCorrectTokenValue() throws Exception {
        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":100,\"token\" : \"\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturnOK_WhenDecreasingBalanceWithValidToken() throws Exception {

        String token = tokenService.getToken("1");

        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":100,\"token\" : \"" + token + "\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }


    @Test
    public void shouldReturn401_WhenDecreasingBalanceWithInValidToken2() throws Exception {

        String token = tokenService.getToken("1");
        String secondToken = tokenService.getToken("1");

        this.mvc.perform(post(DECREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":100,\"token\" : \"" + token + "\"}"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(""));
    }

}
