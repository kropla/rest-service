package com.kropla.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by com.kropla.kropla on 31.12.16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest

@ActiveProfiles("scratch")
public class BalanceIncreaseTest {
    private static final String INCREASE_BALANCE_URL = "/balance/increase/user/";

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void shouldReturnOkWhenIncreasingBalance() throws Exception {
        this.mvc.perform(post(INCREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":102}"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturn422WhenIncreasingBalanceWithWrongValue() throws Exception {
        this.mvc.perform(post(INCREASE_BALANCE_URL + "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":\"www\"}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string(""));
    }


    @Test
    public void shouldReturnWhenIncreasingBalanceWithNonExistingUser() throws Exception {
        this.mvc.perform(post(INCREASE_BALANCE_URL + "12")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"value\":10}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().string(""));
    }
}
