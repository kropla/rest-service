package com.kropla.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by com.kropla.kropla on 31.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest

@ActiveProfiles("scratch")
public class TokenControllerTest {
    private static final String GET_TOKEN_URL = "/tokens/user/";

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }


    @Test
    public void shouldReturnTokenForExistingUser() throws Exception {

        this.mvc.perform(post(GET_TOKEN_URL + "1"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.*", hasSize(1)))
                .andExpect(jsonPath("$.token", notNullValue()));
    }


    @Test
    public void shouldReturn405_ifUsingGet() throws Exception {

        this.mvc.perform(get(GET_TOKEN_URL + "1"))
                .andExpect(status().isMethodNotAllowed());
    }

}
