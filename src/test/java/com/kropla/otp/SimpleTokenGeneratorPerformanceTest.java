package com.kropla.otp;

import org.junit.Test;

/**
 * Created by Maciej_Kroplewski on 1/4/2017.
 */
public class SimpleTokenGeneratorPerformanceTest {

    @Test
    public void overallTimeForThousendCallsForTokenGeneration(){
        long[] resultsForTests = new long[4];
        SimpleTokenGenerator stg = new SimpleTokenGenerator();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++){
            String token = stg.generateToken();
        }
        long end = System.currentTimeMillis();
        resultsForTests[0] = (end - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++){
            String token = stg.generateToken();
        }
        end = System.currentTimeMillis();
        resultsForTests[1] = (end - start);


        start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++){
            String token = stg.generateToken2();
        }
        end = System.currentTimeMillis();
        resultsForTests[2] = (end - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++){
            String token = stg.generateToken2();
        }
        end = System.currentTimeMillis();
        resultsForTests[3] = (end - start);

        for (long l : resultsForTests){
            System.out.println("DEBUG: Logic took " + l + " MilliSeconds");
        }

    }


    @Test
    public void overallTimeFor100ThousandCallsForTokenGeneration(){
        long[] resultsForTests = new long[4];
        SimpleTokenGenerator stg = new SimpleTokenGenerator();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++){
            String token = stg.generateToken();
            System.out.print(token + "|");
        }
        long end = System.currentTimeMillis();
        resultsForTests[0] = (end - start);
        System.out.println();

        start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++){
            String token = stg.generateToken();
            System.out.print(token+ "|");
        }
        end = System.currentTimeMillis();
        resultsForTests[1] = (end - start);
        System.out.println();



        start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++){
            String token = stg.generateToken2();
            System.out.print(token+ "|");
        }
        end = System.currentTimeMillis();
        resultsForTests[2] = (end - start);
        System.out.println();

        start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++){
            String token = stg.generateToken2();
            System.out.print(token+ "|");
        }
        end = System.currentTimeMillis();
        resultsForTests[3] = (end - start);
        System.out.println();

        for (long l : resultsForTests){
            System.out.println("DEBUG: Logic took " + l + " MilliSeconds");
        }

    }
}