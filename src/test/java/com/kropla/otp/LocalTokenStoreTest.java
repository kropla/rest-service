package com.kropla.otp;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by com.kropla.kropla on 02.01.17.
 */
public class LocalTokenStoreTest {

    @Test
    public void shouldReturnTrue_forValidToken(){
        LocalTokenStore otp = new LocalTokenStore();
        String token = "ABCDEF";
        String userId = "11";

        otp.putToken(userId,token);

        assertTrue(otp.isTokenValid(userId,token));
    }

    @Test
    public void shouldReturnFalse_forInValidTokenNotExistingInTokenStore(){
        LocalTokenStore otp = new LocalTokenStore();
        String token = "ABCDEF";
        String invalidToken = "ABCDEE";
        String userId = "11";

        otp.putToken(userId,token);

        assertTrue(!otp.isTokenValid(userId,invalidToken));
    }

    @Test
    public void shouldReturnFalse_forNullToken(){
        LocalTokenStore otp = new LocalTokenStore();
        String token = null;
        String userId = "11";

        otp.putToken(userId,token);

        assertTrue(!otp.isTokenValid(userId,token));
    }
}