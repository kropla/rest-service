package com.kropla.otp;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by com.kropla.kropla on 01.01.17.
 */
public class SimpleTokenGeneratorTest {

    @Test
    public void shouldGenerateToken() {
        SimpleTokenGenerator tokenGenerator = new SimpleTokenGenerator();

        String token = tokenGenerator.generateToken();

        assertNotNull(token);
        assertTrue(!token.isEmpty());
        assertTrue(token.length() == 6);
    }

    @Test
    public void secondTokenShouldBeDifferentThanFirst() {
        SimpleTokenGenerator tokenGenerator = new SimpleTokenGenerator();

        String firstToken = tokenGenerator.generateToken();
        String secondToken = tokenGenerator.generateToken();

        assertNotNull(firstToken);
        assertTrue(!firstToken.isEmpty());

        assertNotNull(secondToken);
        assertTrue(!secondToken.isEmpty());

        assertTrue(!firstToken.equals(secondToken));
    }

}