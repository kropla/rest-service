package com.kropla.json;

import org.junit.Test;
import org.springframework.http.HttpHeaders;

import static org.junit.Assert.*;

/**
 * Created by com.kropla.kropla on 02.01.17.
 */
public class JsonHeaderTest {
    @Test
    public void shouldReturnJsonHeader(){
        HttpHeaders header = new HttpHeaders();
        header.add("Content-Type", "application/json; charset=UTF-8");

        assertTrue(header.equals(JsonHeader.getJsonHeader()));
    }
}