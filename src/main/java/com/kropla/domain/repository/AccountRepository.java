package com.kropla.domain.repository;

import com.kropla.domain.entity.Account;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */

public interface AccountRepository extends Repository<Account, String> {
    Account findByUserId(String userId);

    @Modifying
    @Query("update Account acc set acc.balance = ?1 where acc.userId = ?2")
    void setBalance(int balance, String userId);
}
