package com.kropla.domain.repository;

import com.kropla.domain.entity.Transaction;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
public interface TransactionRepository  extends Repository<Transaction, Integer> {
    List<Transaction> findByUseridOrderById(String userId, Sort sort);

    Transaction save(Transaction tran);
}
