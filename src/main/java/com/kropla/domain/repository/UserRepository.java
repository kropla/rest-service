package com.kropla.domain.repository;

import com.kropla.domain.entity.User;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by com.kropla.kropla on 01.01.17.
 */
public interface UserRepository extends Repository<User, String> {
    User findById(String userId);
    User save(User user);
    List<User> findAll();
}
