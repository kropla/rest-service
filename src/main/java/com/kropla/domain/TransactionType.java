package com.kropla.domain;


import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by com.kropla.kropla on 28.12.16.
 */
public enum TransactionType {
    I("increase"), D("decrease");

    private String type;

    private TransactionType(String type) {
        this.type = type;
    }

    @JsonValue
    public String getType() {
        return type;
    }

}
