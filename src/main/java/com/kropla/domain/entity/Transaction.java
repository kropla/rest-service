package com.kropla.domain.entity;

import com.kropla.domain.TransactionType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by com.kropla.kropla on 28.12.16.
 */
@Entity
@Table(
        name = "transactions"
)
public class Transaction {

    @Id
    @JsonIgnore
    @GeneratedValue
    private Integer id;

    @JsonIgnore
    @Column(name = "accId", nullable = false)
    private int accountid;

    @JsonIgnore
    @Column(name = "userId", nullable = false)
    private String userid;

    @Enumerated(EnumType.STRING)
    @Column(name = "trantype", nullable = false)
    private TransactionType type;

    @Column(nullable = false)
    private int value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAccountid() {
        return accountid;
    }

    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
