package com.kropla.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by com.kropla.kropla on 28.12.16.
 */
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @JsonIgnore
    private int id;

    @Column
    @JsonIgnore
    private String userId;

    @Column
    @JsonProperty("value")
    private int balance;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
