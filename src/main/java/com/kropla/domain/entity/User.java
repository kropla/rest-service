package com.kropla.domain.entity;

import javax.persistence.*;

/**
 * Created by com.kropla.kropla on 28.12.16.
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @Column
    private String id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
