package com.kropla.json;

import org.springframework.http.HttpHeaders;

/**
 * Created by com.kropla.kropla on 01.01.17.
 */
public class JsonHeader {

    public static HttpHeaders getJsonHeader(){
        HttpHeaders header = new HttpHeaders();
        header.add("Content-Type", "application/json; charset=UTF-8");
        return header;
    }
}
