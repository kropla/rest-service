package com.kropla.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
public class Parser {
    public static int getIntValueFromBody(String bodyValue, String nodeName) throws IOException {
        String value = getNodeFromBody(bodyValue, nodeName).asText();
        return Integer.parseInt(value);
    }

    public static String getStringValueFromBody(String bodyValue, String nodeName) throws IOException{
        return getNodeFromBody(bodyValue, nodeName).asText();
    }

    private static JsonNode getNodeFromBody(String bodyValue, String nodeName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(bodyValue) ;
        return node.get(nodeName);
    }
}
