package com.kropla.controller;

import com.kropla.domain.entity.User;
import com.kropla.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by com.kropla.kropla on 03.01.17.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping( method = RequestMethod.POST)
    public ResponseEntity<User> createNewUser(@RequestBody User user){
        userService.createUser(user);
        //User newUser = userService.createUser(lastname, firstName);
        return  new ResponseEntity(user, HttpStatus.CREATED);
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> getListOfUsers(){
        List<User> usersList = userService.getListOfAllUsers();
        return  new ResponseEntity(usersList, HttpStatus.OK);
    }
}
