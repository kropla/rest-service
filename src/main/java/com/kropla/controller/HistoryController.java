package com.kropla.controller;

import com.kropla.service.HistoryService;
import com.kropla.domain.entity.Transaction;
import com.kropla.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static com.kropla.json.JsonHeader.getJsonHeader;

/**
 * Created by com.kropla.kropla on 27.12.16.
 */
@Controller
@RequestMapping("/history")
public class HistoryController {

    private HistoryService transactionHistory;
    private UserService userService;

    @RequestMapping("/user/{userId}")
    public ResponseEntity<String> listLastTransactions(@PathVariable String userId)  {

        if (!userService.checkUserExists(userId)){
            return new ResponseEntity<>("User not exist", HttpStatus.NO_CONTENT);
        }

        List<Transaction> transactions = transactionHistory.getListOfLastTransactions(userId);

        String json = "";
        try {
            json = createJsonNodeWithTransactions(transactions);
        } catch (Exception ex){
            return new ResponseEntity("Exception while creating json node with transactions"
                    , HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(json, getJsonHeader(), HttpStatus.OK);
    }

    private String createJsonNodeWithTransactions(List<Transaction> transactions) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode =  mapper.valueToTree(transactions);
        ObjectNode jsonNode = mapper.createObjectNode();
        jsonNode.putArray("transactions").addAll(arrayNode);
        return mapper.writeValueAsString(jsonNode);
    }

    @Autowired
    public HistoryController(HistoryService transactionHistory, UserService userService) {
        this.transactionHistory = transactionHistory;
        this.userService = userService;
    }
}
