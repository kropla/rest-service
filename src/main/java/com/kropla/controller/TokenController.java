package com.kropla.controller;

import com.kropla.service.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static com.kropla.json.JsonHeader.getJsonHeader;

/**
 * Created by com.kropla.kropla on 29.12.16.
 */
@Controller
@RequestMapping("/tokens/user")
public class TokenController {

    @Autowired
    TokenService tokenService;

    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    public ResponseEntity<String> createTokenForUser(@PathVariable String userId) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jNode = mapper.createObjectNode();
        jNode.put("token",tokenService.getToken(userId));

        return new ResponseEntity<String>(mapper.writeValueAsString(jNode), getJsonHeader(), HttpStatus.CREATED);
    }
}
