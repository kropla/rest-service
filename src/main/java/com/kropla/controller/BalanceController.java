package com.kropla.controller;

import com.kropla.domain.entity.Account;
import com.kropla.json.Parser;
import com.kropla.service.BalanceService;
import com.kropla.service.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by com.kropla.kropla on 27.12.16.
 */
@Controller
@RequestMapping("/balance")
public class BalanceController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceController.class);
    private static final String AMOUNT_NODE_NAME = "value";
    private static final String TOKEN_NODE_NAME = "token";

    @Autowired
    BalanceService balanceService;
    @Autowired
    TokenService tokenService;

    @Transactional(readOnly = true)
    @RequestMapping(value ="/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Account> getUserAccountBalance(@PathVariable String userId){

        Account acc = balanceService.getUserAccount(userId);
        if (acc == null){
            LOGGER.warn("Wrong userId -user nit exists");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(acc, HttpStatus.OK);
    }


    @RequestMapping(value = "/increase/user/{userId}", method = RequestMethod.POST)
    public ResponseEntity<Void> increaseBalanceForUser(@PathVariable String userId, @RequestBody String bodyValue){

        try {
            LOGGER.info("BODY_VALUE" + bodyValue);
            int amount = Parser.getIntValueFromBody(bodyValue, AMOUNT_NODE_NAME);
            LOGGER.info("AMOUNT_VALUE" + amount);

            balanceService.increaseBalance(amount, userId);
        } catch (Exception ex){
            LOGGER.error("Exception during reading amount value for transaction.",ex);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/decrease/user/{userId}", method = RequestMethod.POST)
    public ResponseEntity<Void> decreaseBalanceForUser(@PathVariable String userId, @RequestBody String bodyValue){
        int amount;
        String token = "";
        try {
            amount = Parser.getIntValueFromBody(bodyValue, AMOUNT_NODE_NAME);
            token = Parser.getStringValueFromBody(bodyValue, TOKEN_NODE_NAME);
            LOGGER.debug("TOKEN::" + token);
        } catch (Exception ex){
            LOGGER.error("Exception during processing decrease controller.",ex);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        if (isTokenValueCorrect(token) ) {
            if (tokenService.isTokenValid(userId, token)) {
                balanceService.decreaseBalance(amount, userId);
            } else {
                LOGGER.warn("Token is not valid");
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        } else {
            LOGGER.warn("Malformed token");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


    private boolean isTokenValueCorrect(String token) {
        return token != null && !token.isEmpty();
    }
}
