package com.kropla.otp;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In-memory implementation of TokenStore.  Because this implementation is not distributed, it
 * should only be used for local development and NOT for production environments where high
 * availability is desired.
 */
@Component
public class LocalTokenStore implements TokenStore {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalTokenStore.class);
    private static final int LIFE_TIME_IN_SEC = 10000;
    private Map<String, Token> tokens;
    private ReaperThread reaper;

    public LocalTokenStore() {
        this.tokens = new ConcurrentHashMap<String, Token>();
        reaper = new ReaperThread(LIFE_TIME_IN_SEC);
        reaper.start();
    }

    @Override
    public void putToken(String userId, String token) {
        LOGGER.debug("About to input token:" + token + " for user:" + userId);
        if(userId != null && !userId.isEmpty() && token != null) {
            Token t = new Token(token, System.currentTimeMillis() + LIFE_TIME_IN_SEC);
            tokens.put(userId, t);
            LOGGER.debug("Token inserted to store");
        }
    }

    @Override
    public boolean isTokenValid(String userId, String token) {
        LOGGER.debug("Checking token:" + token + " for user:" + userId);
        boolean tokenIsValid = false;
        synchronized (tokens) {
            Token t = tokens.get(userId);
            LOGGER.debug("Checking token:" + token + " from Map:" + t);
            if (tokenIsValid(t) && t.value.equals(token)){
                tokenIsValid = true;
            }
            removeTokenForUser(userId,t);
        }
        return tokenIsValid;
    }

    private void removeTokenForUser(String username, Token t) {
        if (t != null){
            tokens.remove(username);
        }
    }

    private boolean tokenIsValid(Token t) {
        LOGGER.debug("token is valid: " + (!checkIfTokenValueIsCorrect(t) && checkIfTokenExpired(t)));
        return !checkIfTokenValueIsCorrect(t) && !checkIfTokenExpired(t);
    }


    private boolean checkIfTokenValueIsCorrect(Token t) {
        LOGGER.debug("token is null: " + (t == null));
        return t == null;
    }


    private boolean checkIfTokenExpired(Token t) {
        LOGGER.debug("token expired: " + (t.expires < System.currentTimeMillis()));
        return t.expires < System.currentTimeMillis();
    }

    private void removeExpired() {
        LOGGER.debug("Remving expired tokens");
        Set<Entry<String, Token>> entries = tokens.entrySet();
        for (Entry<String, Token> entry : entries) {
            LOGGER.debug("Checking entry " + entry.getValue());
            LOGGER.debug("System.currentTimeMillis() " + System.currentTimeMillis());

            if (entry.getValue().expires < System.currentTimeMillis()) {
                LOGGER.debug("TOKEN " + entry.getValue() +
                        " for userId:"+entry.getKey() +
                        " will be removed");
                tokens.remove(entry.getKey());
            }
        }
    }



    private class ReaperThread extends Thread {

        public ReaperThread(int lifeTime) {
            setDaemon(true);
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(LIFE_TIME_IN_SEC);
                    removeExpired();
                } catch (InterruptedException e) {
                    // Do nothing
                }
            }
        }
    }
}