package com.kropla.otp;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
@Component
public class CommonTokenGenerator implements TokenGenerator {
    @Override
    public String generateToken() {
        return RandomStringUtils.randomAlphanumeric(6);
    }
}
