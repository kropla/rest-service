package com.kropla.otp;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
public interface TokenStore {
    public void putToken(String username, String token);

    /**
     * Determines if a token is valid.  If the token is valid, this method
     * should return true, and invalidate the underlying token.
     * Two successive calls to this method with the same username and
     * password should not return true.
     */
    public boolean isTokenValid(String username, String token);
}