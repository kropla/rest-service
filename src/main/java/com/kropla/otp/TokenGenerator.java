package com.kropla.otp;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
/**
 * Implementations of this interface generate random token strings.
 */
public interface TokenGenerator {

    /**
     * Generates a random token.
     */
    public String generateToken();
}