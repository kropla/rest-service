package com.kropla.otp;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */

import java.security.SecureRandom;
import java.util.Random;

/**
 * Generates random OTP tokens using the letters A-Z and digits 0-9.
 */
public class SimpleTokenGenerator implements TokenGenerator {
    private static final char[] CHARS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
            '4', '5', '6', '7', '8', '9'};
    private static final int LENGTH = 6;
    private Random rand = new SecureRandom();

    @Deprecated
    public String generateToken2() {

        this.rand = new SecureRandom();
        StringBuilder sb = new StringBuilder(LENGTH);
        for (int i = 0; i < LENGTH; i++) {
            sb.append(CHARS[rand.nextInt(CHARS.length)]);
        }
        return sb.toString();
    }

    @Override
    public String generateToken() {
        StringBuilder sb = new StringBuilder(LENGTH);
        for (int i = 0; i < LENGTH; i++) {
            sb.append(CHARS[rand.nextInt(CHARS.length)]);
        }
        return sb.toString();
    }
}