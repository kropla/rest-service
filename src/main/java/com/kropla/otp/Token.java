package com.kropla.otp;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
public class Token {
    public final String value;
    public final long expires;

    public Token(String value, long expires) {
        this.value = value;
        this.expires = expires;
    }

    @Override
    public String toString() {
        return "Token{" +
                "value='" + value + '\'' +
                ", expires=" + expires +
                '}';
    }
}