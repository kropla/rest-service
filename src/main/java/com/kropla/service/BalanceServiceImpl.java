package com.kropla.service;

import com.kropla.domain.entity.Account;
import com.kropla.domain.repository.AccountRepository;
import com.kropla.domain.TransactionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by com.kropla.kropla on 29.12.16.
 */
@Component
@Transactional
public class BalanceServiceImpl implements BalanceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceServiceImpl.class);

    private AccountRepository accRepo;

    @Autowired
    private HistoryService historyService;

    public BalanceServiceImpl(AccountRepository accRepo) {
        this.accRepo = accRepo;
    }


    @Override
    public Account getUserAccount(String userId) {
        Account acc = accRepo.findByUserId(userId);
        return acc;
    }


    @Override
    public void increaseBalance(int value, String userId) {
        Account acc = accRepo.findByUserId(userId);
        int balance = acc.getBalance() + value;
        accRepo.setBalance(balance,userId);
        historyService.saveTransaction(userId, acc.getId(), TransactionType.I, value);
    }

    @Override
    public void decreaseBalance(int value, String userId) {
        Account acc = accRepo.findByUserId(userId);
        int balance = acc.getBalance() - value;
        accRepo.setBalance(balance,userId);
        historyService.saveTransaction(userId, acc.getId(), TransactionType.D, value);
    }
}
