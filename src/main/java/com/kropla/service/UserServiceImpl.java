package com.kropla.service;

import com.kropla.domain.entity.User;
import com.kropla.domain.repository.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by com.kropla.kropla on 29.12.16.
 */
@Component
@Transactional
public class UserServiceImpl implements UserService {

    UserRepository userRepo;

    public UserServiceImpl(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public boolean checkUserExists(String userId) {
        User user = userRepo.findById(userId);
        return user != null;
    }

    @Override
    public User createUser(User user) {
        return userRepo.save(user);
    }

    @Override
    public List<User> getListOfAllUsers() {
        return userRepo.findAll();
    }
}
