package com.kropla.service;

import com.kropla.domain.entity.User;

import java.util.List;

/**
 * Created by com.kropla.kropla on 01.01.17.
 */
public interface UserService {
    boolean checkUserExists(String userId);
    User createUser(User user);
    List<User> getListOfAllUsers();
}
