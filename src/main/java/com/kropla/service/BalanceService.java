package com.kropla.service;

import com.kropla.domain.entity.Account;

/**
 * Created by com.kropla.kropla on 29.12.16.
 */
public interface BalanceService {

    Account getUserAccount(String userId);

    void increaseBalance(int value, String userId);
    void decreaseBalance(int value, String userId);
}
