package com.kropla.service;

/**
 * Created by com.kropla.kropla on 29.12.16.
 */
public interface TokenService {
    String getToken(String userId);
    boolean isTokenValid(String userId, String token);
}
