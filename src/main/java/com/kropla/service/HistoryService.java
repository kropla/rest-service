package com.kropla.service;

import com.kropla.domain.entity.Transaction;
import com.kropla.domain.TransactionType;

import java.util.List;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
public interface HistoryService {
    List<Transaction> getListOfLastTransactions(String userId);

    void saveTransaction(String userId, int accId, TransactionType i, int value);
}
