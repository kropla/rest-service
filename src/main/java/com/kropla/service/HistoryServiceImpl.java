package com.kropla.service;


import com.kropla.domain.entity.Transaction;
import com.kropla.domain.repository.TransactionRepository;
import com.kropla.domain.TransactionType;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by com.kropla.kropla on 29.12.16.
 */
@Component
@Transactional
public class HistoryServiceImpl implements HistoryService {

    TransactionRepository tranRepo;

    public HistoryServiceImpl(TransactionRepository tranRepo) {
        this.tranRepo = tranRepo;
    }

    @Override
    public List<Transaction> getListOfLastTransactions(String userId) {
        return tranRepo.findByUseridOrderById(userId, new Sort("id"));
    }

    @Override
    public void saveTransaction(String userId, int accId, TransactionType type, int value) {
        Transaction tran = new Transaction();
        tran.setUserid(userId);
        tran.setAccountid(accId);
        tran.setType(type);
        tran.setValue(value);

        tranRepo.save(tran);
    }
}
