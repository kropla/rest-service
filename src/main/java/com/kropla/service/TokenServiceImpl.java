package com.kropla.service;

import com.kropla.otp.TokenGenerator;
import com.kropla.otp.TokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by com.kropla.kropla on 30.12.16.
 */
@Component
public class TokenServiceImpl implements TokenService{
    @Autowired
    TokenGenerator tokenGenerator;

    @Autowired
    TokenStore tokenStore;

    @Override
    public String getToken(String userId) {
        String token = tokenGenerator.generateToken();
        tokenStore.putToken(userId, token);
        return token;
    }

    @Override
    public boolean isTokenValid(String userId, String token) {
        return tokenStore.isTokenValid(userId,token);
    }
}
