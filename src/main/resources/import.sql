INSERT INTO users(id, first_name, last_name) VALUES (1, 'maciej', 'kropla')
INSERT INTO users(id, first_name, last_name) VALUES (2, 'marcin', 'marka')
INSERT INTO users(id, first_name, last_name) VALUES (3, 'adam', 'wiadro')

INSERT INTO accounts(id, balance, user_id) VALUES (1, 100, 1)
INSERT INTO accounts(id, balance, user_id) VALUES (2, 5, 3)
INSERT INTO accounts(id, balance, user_id) VALUES (3, 200, 2)

INSERT INTO transactions(id, acc_id, user_id, trantype, value) VALUES (1, 1, 1, 'I',100)
INSERT INTO transactions(id, acc_id, user_id, trantype, value) VALUES (2, 1, 1, 'I',50)
INSERT INTO transactions(id, acc_id, user_id, trantype, value) VALUES (3, 1, 1, 'I',205)
INSERT INTO transactions(id, acc_id, user_id, trantype, value) VALUES (4, 1, 1, 'D',103)
INSERT INTO transactions(id, acc_id, user_id, trantype, value) VALUES (5, 2, 3, 'D',5)
INSERT INTO transactions(id, acc_id, user_id, trantype, value) VALUES (6, 2, 3, 'I',10)

--http://localhost:8080/bestBank/h2-console/
--jdbc:h2:mem:testdb