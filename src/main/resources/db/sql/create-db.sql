CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  firstname VARCHAR(50),
  lastname  VARCHAR(50)
);

CREATE TABLE accounts (
  id INTEGER PRIMARY KEY,
  userId INTEGER,
  balance INTEGER
);

CREATE TABLE transactions (
  id INTEGER PRIMARY KEY,
  accId INTEGER,
  trantype VARCHAR(20),
  value INTEGER
);
