
INSERT INTO users VALUES (1, 'maciej', 'com.kropla.kropla');
INSERT INTO users VALUES (2, 'marcin', 'marka');
INSERT INTO users VALUES (3, 'adam', 'wiadro');

INSERT INTO accounts VALUES (1, 1, 100);
INSERT INTO accounts VALUES (2, 3, 5);
INSERT INTO accounts VALUES (3, 2, 200);

INSERT INTO transactions VALUES (1, 1, 'I',100);
INSERT INTO transactions VALUES (2, 1, 'I',50);
INSERT INTO transactions VALUES (3, 1, 'I',205);
INSERT INTO transactions VALUES (4, 1, 'D',103);
INSERT INTO transactions VALUES (5, 2, 'D',5);
INSERT INTO transactions VALUES (6, 2, 'I',10);